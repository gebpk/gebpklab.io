# GGEB Gatsby Frontend.

This is the frontend of GGEB's website. Its gatsby based and it requires that Strapi CMS
is ran before it is started with the following command. For further details on Gatsby
refer to its official documentation.

```
npm run develop
```

# Deploying process

### First step
```
npm run deploy-gl
```
This command will update https://gebpk.gitlab.io/

### Second step
This will as you for password, provide the SSH password in documentation email. 
```
npm run deploy-hostgator
```
This command will update http://ggebpakistan.com/