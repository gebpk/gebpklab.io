import React, { HTMLAttributes } from 'react';

import './logo.scss';
const logo = require('./images/usman-23-11-2020.png') as string;

export default (props: HTMLAttributes<any>) =>
    <img src={logo} className="logo" {...props} />
