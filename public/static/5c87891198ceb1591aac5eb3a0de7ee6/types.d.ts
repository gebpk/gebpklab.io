export type Image = {
    caption: string
    file: {
        localFile: {
            publicURL: string
            childImageSharp: {
                fluid: {
                    aspectRatio: number
                    src: string
                }
            }
        }
    }
}