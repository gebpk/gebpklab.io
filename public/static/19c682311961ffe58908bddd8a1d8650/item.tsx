import React from 'react';
import { Col, Row } from 'reactstrap';
import { Truncate } from '../text';
import { formateDate } from '../text/formating';
import { Report } from './query';

const initialSummaryLength = 350;

type Props = {
    report: Report
    truncateSummary?: boolean
    excludeTitle?: boolean
}

export default ({ report, truncateSummary = false, excludeTitle = false }: Props) => {
    return (
        <Row>
            <Col md="4">
                <img
                    src={report.cover.localFile.publicURL}
                    className="img-fluid"
                />
            </Col>
            <Col md="8">
                {!excludeTitle ?
                    <>
                        <h4 className="h5">{report.title}</h4>
                    </>
                    : null}
                <p>
                    Send us your email address and we will mail you the download link for this report.
                </p>
            </Col>
        </Row>
    );
}